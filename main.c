/**
 * @file      main.c
 * @author    wzzlyzdn (wzzlyzdn@163.com)
 * @brief     主函数文件
 * @version   0.1
 * @date      2022-08-17
 * 
 * @copyright Copyright (c) 2022 wzzlyzdn
 * 
 * @note      历史记录: 
 *            - 
 * @warning   
 * @par       修改记录: 
 * <table>
 * <tr><th>date          <th>Version    <th>Author      <th>Description    </tr>
 * <tr><td>2022-08-17    <td> 0.1       <td>wzzlyzdn       <td>创建初始版本    </tr>
 * </table>
 */
#include "server_client.h"
#include "server.h"
#include "udp.h"

UDP_INFO sudpip = {"0.0.0.0", 12500, SERVER_ID};
UDP_INFO cudpip = {"0.0.0.0", 12500, CLIENT_ID};

/**
 * @fn        int main(int argc, char *argv[])
 * @brief     主函数 - 模式选择
 * 
 * @param     [in] argc      
 * @param     [in] argv      
 * 
 * @return    int            
 */

int main(int argc, char *argv[])
{
    int mode = 1;
    DEBUG("请选择模式：\n按1. 选择 TCP\n按2. 选择 UDP\n");
    scanf("%d",&mode);
    if(argc > 1)
    {
        if(1 == mode)
        {
            tcp_client_main(argc, argv);
        }
        else if(2 == mode)
        {
            sprintf(cudpip.ip, "%s", argv[1]);
            udp_client_main(cudpip);
        }
        else
        {
            DEBUG("err : 参数错误\n");
        }
    }
    else if(1 == argc)
    {
        if(1 == mode)
        {
            tcp_server_main(argc, argv);
        }
        else if(2 == mode)
        {
            udp_server_main(sudpip);
        }
        else
        {
            DEBUG("err : 参数错误\n");
        }
    }
    else
    {
        DEBUG("err : 错误\n");
    }
    return 0;
}