LIB_NAME ?= FileTransfer# 库文件名ProjectName
EXE ?= FileTransfer# 可执行文件名ProjectName

PWD := $(shell pwd)# /data1/wzzlyzdn/work/ProjectName
BUILD_PATH := $(PWD)/build# /data1/wzzlyzdn/work/ProjectName/build
LIB_PATH := $(PWD)/lib# 动态链接库和静态链接库路径 /data1/wzzlyzdn/work/ProjectName/bin
SOURCE_PATH := $(PWD)/source
BIN_PATH := $(PWD)/bin#二进制文件路径
SHARE_NAME  ?= lib$(LIB_NAME).so# libProjectName.o
STATIC_NAME ?= lib$(LIB_NAME).a# libProjectName.a

SHARE_EXE ?= share_$(EXE)# 动态链接库生成的可执行文件 share_ProjectName
STATIC_EXE ?= static_$(EXE)# 静态链接库生成的可执行文件 static_ProjectName

SHARE_LIB := $(LIB_PATH)/$(SHARE_NAME)# /data1/wzzlyzdn/work/ProjectName/lib/libProjectName.so
STATIC_LIB := $(LIB_PATH)/$(STATIC_NAME)# /data1/wzzlyzdn/work/ProjectName/lib/libProjectName.a

SHARE_TARGET := $(BIN_PATH)/$(SHARE_EXE)# 动态链接库可执行文件 /data1/wzzlyzdn/work/ProjectName/bin/share_ProjectName
STATIC_TARGET := $(BIN_PATH)/$(STATIC_EXE)# 静态链接库可执行文件 /data1/wzzlyzdn/work/ProjectName/bin/static_ProjectName

INCL := -I./include/
LIB:= -L./lib/


SOURCE := $(wildcard $(SOURCE_PATH)/*.c)
OBJS := $(patsubst %.c, $(BUILD_PATH)/%.o, $(notdir $(SOURCE)))

all: $(STATIC_LIB) $(SHARE_LIB) $(STATIC_TARGET) $(SHARE_TARGET) 

test:
	@echo "OBJS" $(OBJS)
	@echo "SOURCE" $(SOURCE)
#生成动态库 libProjectName.so
$(SHARE_LIB):$(OBJS)
	$(CC) -shared -fPIC $(INCL) -o $(SHARE_LIB) $(SOURCE)
	
#生成静态库 libProjectName.a
$(STATIC_LIB):$(OBJS)
	ar cr $(STATIC_LIB) $(OBJS)
	ranlib $(STATIC_LIB)
# ranlib 相当于 ar -s

#生成二进制文件(*.o)	
$(OBJS): $(BUILD_PATH)/%.o:$(SOURCE_PATH)/%.c
	$(CC) -c $(INCL) $< -o $@ -lpthread

#生成动态二进制文件	
$(SHARE_TARGET):main.c
# $(CC) main.c $(INCL) $(LIB) -l$(LIB_NAME) -rpath -o $(SHARE_TARGET)
	$(CC) $< $(INCL) $(LIB) -l$(LIB_NAME) -Wl,-rpath,./lib -o $(SHARE_TARGET) -lpthread

#生成静态二进制文件
$(STATIC_TARGET):main.c
	$(CC) $< -o $(STATIC_TARGET) $(INCL) $(LIB) -l$(LIB_NAME) -static -lpthread

clean:
	rm -rf $(BUILD_PATH)/*.o $(LIB_PATH)/*.{a,so}
	rm -rf $(STATIC_TARGET) $(SHARE_TARGET)
	
.PHONY: all clean
