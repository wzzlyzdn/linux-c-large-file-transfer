/**
 * @file      server.h
 * @author    wzzlyzdn (wzzlyzdn@163.com)
 * @brief
 * @version   0.1
 * @date      2022-08-04
 *
 * @copyright Copyright (c) 2022 wzzlyzdn
 *
 * @note      历史记录:
 *            - 创建初始版本
 *            - 增加文件注释
 * @warning
 * @par       修改记录:
 * <table>
 * <tr><th>date          <th>Version    <th>Author      <th>Description    </tr>
 * <tr><td>2022-08-04    <td> 0.1       <td>wzzlyzdn       <td>创建初始版本    </tr>
 * <tr><td>2022-08-12    <td> 0.1       <td>wzzlyzdn       <td>增加文件注释    </tr>
 * </table>
 */
#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>

#define DEBUG(format, ...) fprintf(stdout, format, ##__VA_ARGS__) 

#define BUFFER_SIZE 4096
/* exit code 退出代码 */
#define SERVER_RUNNING 0     /* 服务器运行正常 代码 0 */
#define SERVER_MAINTENANCE 1 /* 服务器维护 退出代码 1 */
#define CLIENT_EXIT 2        /* 客户端主动退出 退出代码 2 */

/* error_code 错误代码 */
#define SOCKET_CREATE_ERR -1 /* socket 创建失败 错误代码 -1 */
#define BIND_ERR -2          /* bind 绑定失败 错误代码 -2 */
#define LISTEN_ERR -3        /* listen 监听失败 错误代码 -3 */
#define ACCEPT_ERR -4        /* accept 接收客户端请求失败 错误代码 -4 */
#define THREAD_CREATE_ERR -5 /* pthread_create 线程创建失败 错误代码 -5 */

/**
 * @struct    CLIENT_INFO    [server.h]
 * @brief     保存客户端 sockfd 以及 客户端名称 name
 * 
 */
typedef struct
{
    int sockfd_client; /**< accept 返回的 客户端 (client) 的 socket */
    char name[28];     /**< 客户端 (client) 的标识名 */
} CLIENT_INFO;



typedef struct IPPORT
{
    char ip[32];/**< IP 地址 */
    int port;/**< 端口号 */
    int ui;/**< 服务器客户端标识 */
} UDPID;

#endif /* !SERVER_H */
