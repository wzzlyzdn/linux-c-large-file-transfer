/**
 * @file      udp.h
 * @author    wzzlyzdn (wzzlyzdn@163.com)
 * @brief     UDP 头文件
 * @version   0.1
 * @date      2022-08-16
 * 
 * @copyright Copyright (c) 2022 wzzlyzdn
 * 
 * @note      历史记录: 
 *            - 
 * @warning   
 * @par       修改记录: 
 * <table>
 * <tr><th>date          <th>Version    <th>Author      <th>Description    </tr>
 * <tr><td>2022-08-16    <td> 0.1       <td>wzzlyzdn       <td>创建初始版本    </tr>
 * </table>
 */

#ifndef UDP_H
#define UDP_H

#define SERVER_ID 0
#define CLIENT_ID 1

typedef struct
{
    char ip[40]; /**< ipv4 最大占15字节 ipv6 最大占39字节 */
    int port;    /**< 端口号 */
    int id;      /**< 服务器客户端标识 */ 
} UDP_INFO;

typedef struct
{
    struct sockaddr_in addr;
    int sockfd;
}SOCKPACK;


int UDP_Init(UDP_INFO udp, SOCKPACK *sock_pack);
void *UDP_Recv(void *arg);
void *UDP_Send(void *arg);
void save_udp_info(UDP_INFO *info, struct sockaddr_in my_addr);
int test();

int udp_server_main(UDP_INFO udpip);
int udp_client_main(UDP_INFO udpip);

#endif/* !UDP_H */
