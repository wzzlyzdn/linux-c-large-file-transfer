# 文件传输介绍

## 编译

- 在文件目录下执行脚本 setup.sh
    ```shell
    $ chmod +x setup.sh
    $ ./setup.sh
    $ tree
    .
    ├── bin
    │   ├── share_FileTransfer
    │   └── static_FileTransfer
    ├── build
    │   ├── filetransfer.o
    │   ├── md5sum.o
    │   ├── tcp_client.o
    │   ├── tcp_server.o
    │   ├── udp_client.o
    │   ├── udp.o
    │   └── udp_server.o
    ├── doc
    │   ├── Doxyfile
    │   └── readme.md
    ├── include
    │   ├── md5sum.h
    │   ├── server_client.h
    │   ├── server.h
    │   └── udp.h
    ├── lib
    │   ├── libFileTransfer.a
    │   └── libFileTransfer.so
    ├── main.c
    ├── Makefile
    ├── setup.sh
    └── source
        ├── filetransfer.c
        ├── md5sum.c
        ├── tcp_client.c
        ├── tcp_server.c
        ├── udp.c
        ├── udp_client.c
        └── udp_server.c
    ```
- 执行脚本后，在 ./bin 目录下生成两个可执行文件，share 开头的链接了动态链接库 **libFileTransfer.so** ，static 开头的可执行文件链接了静态链接库 **libFileTransfer.a**


## 使用

- 在当前目录下执行 **static_FileTransfer**，此文件为服务端客户端一体程序

### 服务端

```shell
# 运行服务端
$ ./bin/static_FileTransfer
请选择模式：
按1. 选择 TCP
按2. 选择 UDP

# 选择协议 开启服务器
```

### 客户端

```shell
# 运行客户端 传递 IP 地址
$ ./bin/static_FileTransfer 192.168.xxx.xxx
请选择模式：
按1. 选择 TCP
按2. 选择 UDP
# 选择协议 开启客户端
```

#### 文件传输

- 在 TCP 模式下输入指令进行文件传输操作

  - ``` $FILE_TRANSFER_BREAK$ ```，中断文件传输
  - ``` $FILE_TRANSFER_CONTINUE$ ```，继续文件传输
  - ``` $FILE_LIST_REQUEST$ ```，请求服务器文件列表
  - ``` $FILE_TRANSFER$ ```，传输文件

- 演示

```shell
# 服务端 发送指令
~/TCP_UDP$ ./bin/static_FileTransfer 192.168.xxx.xxx
# 在程序中输入 $FILE_LIST_REQUEST$ 请求文件列表
$FILE_LIST_REQUEST$
服务器：
.    ..    source    lib    build    setup.sh    Makefile    bin    main.c    include    doc

# 在程序中输入 $FILE_TRANSFER$ 根据提示输入文件名，进行文件传输
$FILE_TRANSFER$
输入文件名
main.c
文件: main.c 接收成功 耗时 0.116 秒
发送 main.c MD5: fbf29334245779c12c7f6f837dcad78f
接收 main.c MD5: fbf29334245779c12c7f6f837dcad78f
MD5 校验成功！

# 在程序中输入 $FILE_TRANSFER_BREAK$ 文件传输中断
$FILE_TRANSFER_BREAK$
传输中断
文件: tar.zip 接收中断 耗时 19.162 秒

# 在程序中输入 $FILE_TRANSFER_CONTINUE$ 根据提示输入继续传输的文件名进行继续传输

$FILE_TRANSFER_CONTINUE$
输入文件名
tar.zip
文件: tar.zip 接收成功 耗时 32.682 秒
发送 tar.zip MD5: 25671bb58a7dba93058b3918ffb2caaa
接收 tar.zip MD5: 25671bb58a7dba93058b3918ffb2caaa
MD5 校验成功！

# 退出 发送 quit 退出程序
quit
```




