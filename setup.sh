#!/bin/bash
rm -rf *.exe
rm -rf .vscode
rm -rf *.git*
mkdir -p build
mkdir -p lib
mkdir -p bin
mkdir -p include
mkdir -p source
mkdir -p doc

mv `ls | grep .c$ | grep -v main | grep -v doc` ./source
mv *.h ./include
mv *.doc* *.md *readme* *.xls* -t ./doc

make