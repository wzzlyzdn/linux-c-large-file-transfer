/**
 * @file      server_client.h
 * @author    wzzlyzdn (wzzlyzdn@163.com)
 * @brief     服务器客户端协议
 * @version   0.1
 * @date      2022-08-04
 *
 * @copyright Copyright (c) 2022 wzzlyzdn
 *
 * @note      历史记录:
 *            - 创建初始版本
 *            - 增加文件注释
 * @warning
 * @par       修改记录:
 * <table>
 * <tr><th>date          <th>Version    <th>Author      <th>Description    </tr>
 * <tr><td>2022-08-04    <td> 0.1       <td>wzzlyzdn       <td>创建初始版本    </tr>
 * <tr><td>2022-08-12    <td> 0.2       <td>wzzlyzdn       <td>增加文件注释    </tr>
 * </table>
 */

#ifndef SERVER_CLIENT_H
#define SERVER_CLIENT_H
#include <time.h>
#include <stdio.h>

#define __MY_DEBUG__
#ifdef __MY_DEBUG__

#ifdef __DEBUG_INFO__
#define DEBUG_INFO(format, ...) fprintf(stdout, "[ $DEBUG$ ] File < %s:%d > \t" format, __FILE__, __LINE__, ##__VA_ARGS__) /* 打印调试信息，包含文件名，代码行数 */
#else
#define DEBUG_INFO(format, ...)
#endif/* !__DEBUG_INFO__ */

#define __DEBUG__
#ifdef __DEBUG__
#define DEBUG(format, ...) fprintf(stdout, format, ##__VA_ARGS__)                                                          /* 打印调试信息 */
#endif/* !__DEBUG__ */
#else

#define DEBUG_INFO(format, ...)
#define DEBUG(format, ...)
#endif/* __MY_DEBUG__ */

#define BUFFER_SIZE 4096  /* 缓冲区大小 4K */
#define IP_ADDRESS        /* IP 地址 */
#define SERVER_PORT 19200 /* 服务器 端口 */

#define FILE_TRANSFER 1               /* $FILE_TRANSFER$ */
#define FILE_TRANSFER_READY 2         /* $FILE_TRANSFER_READY$ */
#define FILE_TRANSFER_START 3         /* $FILE_TRANSFER_START$ */
#define FILE_TRANSFER_FINISHED 4      /* $FILE_TRANSFER_FINISHED$ */
#define FILE_LIST_REQUEST 7           /* $FILE_LIST_REQUEST$ */
#define FILE_TRANSFER_BREAK 0         /* $FILE_TRANSFER_BREAK$ */
#define FILE_TRANSFER_CONTINUE 2      /* $FILE_TRANSFER_CONTINUE$ */
#define FILE_TRANSFER_REQUEST 5       /* $FILE_TRANSFER_REQUEST$ */
#define FILE_TRANSFER_REQUEST_ALLOW 6 /* $FILE_TRANSFER_REQUEST_ALLOW$ */

#define FILE_RECV_FINISHED 7

#define FILE_TRANSFER_ERR -1

// static int file_transfer_state = FILE_TRANSFER;

/**
 * @struct    FILE_INFO    [server_client.h]
 * @brief     用于存储通信信息地址
 *            IP
 *            Port
 *            sockfd
 *            文件名
 *            文件大小
 *
 */
// typedef struct DATA_PACKET
typedef struct
{
    int sockfd;         /**< 目标sockfd*/
    int port;           /**< 端口号*/
    long filesize;      /**< 文件大小 字节*/
    char filename[256]; /**< 文件名*/
    char IP[64];        /**< IP 地址*/
} FILE_INFO;

int socket_create(int port);
int c_sockfd(char *ip_addr, int port);

void *file_transform(void *arg);
void *file_receive(void *arg);

void data_packets_combining(FILE_INFO db, char *buf);
void data_packets_parsing(FILE_INFO *db, char *buf);
void display_data_packets(FILE_INFO db);

void display_flie_list(char *buf, int sockfd);
void file_rename(FILE_INFO *db);
void file_info_bzero_except_ip(FILE_INFO *db);
int get_filesize(FILE_INFO *db);

int tcp_server_main(int argc, char *argv[]);
int tcp_client_main(int argc, char *argv[]);

#endif /* !SERVER_CLIENT_H */
